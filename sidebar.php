
				
				<?php // For pages
				if (is_page()) { ?>
				<div class="col mobile-menu">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php 
								// If a Research subpage
								if (is_tree(939) || get_field('menu_select') == "research") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Research', 'bonestheme' ),
										'menu_class' => 'research-nav',
										'theme_location' => 'research-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Research</h3><ul>%3$s</ul>'
									));
								}
								
								// If a People subpage
								if (is_tree(1096) || get_field('menu_select') == "people" ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'People', 'bonestheme' ),
										'menu_class' => 'people-nav',
										'theme_location' => 'people-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>People</h3><ul>%3$s</ul>'
									));
								}
								
								// If a Newsletter subpage
								if (get_field('menu_select') == "newsletter" ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Newsletter', 'bonestheme' ),
										'menu_class' => 'newsletter-nav',
										'theme_location' => 'newsletter-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Newsletter</h3><ul>%3$s</ul>'
									));
								}
								
								// If a Giving subpage
								if ( get_field('menu_select') == "giving" ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Giving', 'bonestheme' ),
										'menu_class' => 'giving-nav',
										'theme_location' => 'giving-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Giving</h3><ul>%3$s</ul>'
									));
								}
								
								// If a About subpage
								if ( get_field('menu_select') == "about" ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>About</h3><ul>%3$s</ul>'
									));
								}
								
								// If a Contact subpage
								if ( get_field('menu_select') == "contact" ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Contact', 'bonestheme' ),
										'menu_class' => 'contact-nav',
										'theme_location' => 'contact-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Contact</h3><ul>%3$s</ul>'
									));
								}
								
								// If a blog post or category
								if ( is_single() || is_category() ) {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'News', 'bonestheme' ),
										'menu_class' => 'news-nav',
										'theme_location' => 'news-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Categories</h3><ul>%3$s</ul>'
									));
								}
								
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if ( is_tree(9999) || is_search() || is_404() || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>About</h3><ul>%3$s</ul>'
									));
								}
							?>
						</nav>
					</div>
				</div>
					
				<?php } ?>