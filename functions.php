<?php
// Load Bones Core
require_once( 'library/bones.php' );
	
// Customize Wordpress Admin
require_once( 'library/admin.php' );

// Recommend Plugins
require_once( 'library/install-plugins.php' );

/*********************
LAUNCH BONES
*********************/

function bones_ahoy() {

	// Allow editor style.
	add_editor_style( 'library/css/editor-style.css');
	
/************* CUSTOM POST TYPES *************/
	
	// Controlled by ACF plugin.
	// Check to see if the plugin is installed first.
	if( function_exists ('acf') ) {
		if(get_field('enable_people', 'option') == "enable") {
			require_once( 'library/post-types/people-post-type.php' );
		}
		if(get_field('enable_courses', 'option') == "enable") {
			require_once( 'library/post-types/courses-post-type.php' );
		}
		if(get_field('enable_books', 'option') == "enable") {
			require_once( 'library/post-types/book-post-type.php' );
		}
		if(get_field('enable_conferences', 'option') == "enable") {
			require_once( 'library/post-types/conference-post-type.php' );
		}
	}
	// If it's not, then just do it
	// Comment out if you don't need something.
	else {
		require_once( 'library/post-types/people-post-type.php' );
		require_once( 'library/post-types/courses-post-type.php' );
		require_once( 'library/post-types/book-post-type.php' );
		require_once( 'library/post-types/conference-post-type.php' );
	}

/************* END *************/

	// launching operation cleanup
	add_action( 'init', 'bones_head_cleanup' );
	// A better title
	add_filter( 'wp_title', 'rw_title', 10, 3 );
	// remove WP version from RSS
	add_filter( 'the_generator', 'bones_rss_version' );
	// clean up gallery output in wp
	add_filter( 'gallery_style', 'bones_gallery_style' );
	// enqueue base scripts and styles
	add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
	// launching this stuff after theme setup
	bones_theme_support();
	// adding sidebars to Wordpress (these are created in functions.php)
	add_action( 'widgets_init', 'bones_register_sidebars' );
	// cleaning up random code around images
	add_filter( 'the_content', 'bones_filter_ptags_on_images' );
} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );

/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 640;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Hero image
add_image_size( 'hero', 1200, 398, true );
// Home Hero image
add_image_size( 'home-hero', 1200, 660, true );
// Content width
add_image_size( 'content-width', 640, 430, true );
// Article thumbnail
add_image_size( 'article-thumb', 110, 70, true );

// Only use certain image sizes if necessary
if( function_exists ('acf') ) {
	if(get_field('enable_people', 'option') == "enable") {
		// Big people photo
		add_image_size( 'people-large', 300, 300, array( 'center', 'top' ) );
		// Small people photo
		add_image_size( 'people-thumb', 140, 140, array( 'center', 'top' ) );
	}
	if(get_field('enable_books', 'option') == "enable") {
		// Large book cover
		add_image_size( 'large-book', 274, 383, true );
		// Medium book cover
		add_image_size( 'medium-book', 101, 157, true );
		// Small book cover
		add_image_size( 'small-book', 60, 90, true );
	}
	if(get_field('enable_conferences', 'option') == "enable") {
		// Speaker photo
		add_image_size( 'speaker-photo', 160, 160, true );
	}
} else {
	// Big people photo
	add_image_size( 'people-large', 280, 280, array( 'center', 'top' ) );
	// Small people photo
	add_image_size( 'people-thumb', 100, 100, array( 'center', 'top' ) );
	// Large book cover
	add_image_size( 'large-book', 274, 383, true );
	// Medium book cover
	add_image_size( 'medium-book', 101, 157, true );
	// Small book cover
	add_image_size( 'small-book', 60, 90, true );
	// Speaker photo
	add_image_size( 'speaker-photo', 160, 160, true );
}

/* 
This makes Wordpress create thumbnails at these 
sizes for every image for you to then use in
theme as needed. 

To add more sizes, simply copy a line and change 
the dimensions & name. As long as you upload a 
"featured image" as large as the biggest set 
width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
	return array_merge( $sizes, array(
		'content-width' => __('Full Content Width'),
	) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'news-sidebar',
		'name' => __( 'News Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for news article feed.', 'bonestheme' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div><a class="btn" href="/category/news/">View All<span class="hidden"> News</span></a>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'events-sidebar',
		'name' => __( 'Event Sidebar', 'bonestheme' ),
		'description' => __( 'Sidebar for event feed.', 'bonestheme' ),
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

	register_sidebar(array(
		'id' => 'widget-1-sidebar',
		'name' => __( 'Homepage Widget 1', 'bonestheme' ),
		'description' => __( 'Widget to be used on homepage.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	
} // don't remove this bracket!

/*********************
ADDITIONAL FUNCTIONS
*********************/

/************* MENU DESCRIPTIONS *********************/

function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', $args->link_after . '</a>' . '<span class="description">' . $item->description . '</span>', $item_output );
    }
    return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );

/************* FILTER MENU *********************/

// Removing classes and replacing anchor with button element

class Filter_Walker extends Walker_Nav_Menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent\n";
	}
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent\n";
	}
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
		$class_names = $value = '';
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="option ' . esc_attr( $class_names ) . '"' : '';
		$output .= $indent . '';
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ' data-filter=".'. esc_attr( $item->xfn   ) .'"';
		$attributes .= ' data-text="'. esc_attr( $item->title   ) .'"';
		$item_output = $args->before;
		$item_output .= '<button'. $attributes . $class_names .'>';
		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
		$item_output .= '</button>';
		$item_output .= $args->after;
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "\n";
	}
}

// Removing all but custom classes
function modify_nav_menu_args( $args ) {
	if( 'faculty-filter' == $args['theme_location'] ) {
		add_filter('nav_menu_css_class', 'discard_menu_classes', 10, 2);
		function discard_menu_classes($classes, $item) {
			return (array)get_post_meta( $item->ID, '_menu_item_classes', true );
		}
	}
	return $args;
}
add_filter( 'wp_nav_menu_args', 'modify_nav_menu_args' );

// Converting menu to drop down
class Dropdown_Walker extends Walker_Nav_Menu {
	function start_lvl(&$output, $depth = 0, $args = array()){
		$indent = str_repeat("\t", $depth); // don't output children opening tag (`<ul>`)
	}
	function end_lvl(&$output, $depth = 0, $args = array()){
		$indent = str_repeat("\t", $depth); // don't output children closing tag
	}
	function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
 		$xfn = '#' !== $item->xfn ? $item->xfn : '';
 		$output .= '<option value=".' . $xfn . '">' . $item->title;
	}	
	function end_el(&$output, $item, $depth = 0, $args = array()){
		$output .= "</option>\n"; // replace closing </li> with the option tag
	}
}

/************* TARGETING PARENT PAGE *********************/

// Be able to target pages by using is_tree(parent-id)

function is_tree($pid) {
	global $post;
	$ancestors = get_post_ancestors($post->$pid);
	$root = count($ancestors) - 1;
	$parent = $ancestors[$root];
	if(is_page() && (is_page($pid) || $post->post_parent == $pid || in_array($pid, $ancestors))) {
		return true;
	}
	else {
		return false;
	}
};

/************* SHORTCODE *********************/

// Remove bad html in shortcode
function shortcode_empty_paragraph_fix($content) {
	$array = array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']'
	);
	$content = strtr($content, $array);
	return $content;
}
add_filter('the_content', 'shortcode_empty_paragraph_fix');

// FAQ Shortcode
function question_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'title' => '',
		),
	$atts ) );
	return '<h5 class="faq topic js-expandmore" data-hideshow-prefix-class="animated"><span class="fas fa-caret-right"></span>'. $title .'</h5><div class="response">' . do_shortcode($content) . '</div>';
}
add_shortcode('question', 'question_shortcode');

/* Code Example
[question title="To be or not to be?"]
That is the question.
[/question]
*/

// Accordion Shortcode
function accordion_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'title' => '',
		),
	$atts ) );
	return '<h5 class="accordion topic js-expandmore" data-hideshow-prefix-class="animated"><span class="fas fa-plus"></span>'. $title .'</h5><div class="response accordion">' . do_shortcode($content) . '</div>';
}
add_shortcode('accordion', 'accordion_shortcode');

/* Code Example
[accordion title="Keep in mind search engines can't see hidden content"]
You should not put important information you want search engines to see in this style.
[/accordion]
*/

// Button Shortcode
function button_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'link' => '',
		),
	$atts ) );
	return '<a href="' . $link . '" class="btn">' . $content . '</a>';
}
add_shortcode('button', 'button_shortcode');

/* Code Example
[button link="http://ucla.edu"]Button Text[/button]
*/

// Notice Shortcode
function notice_shortcode( $atts, $content = null ) {
	extract( shortcode_atts(
		array(
			'title' => '',
		),
	$atts ) );
	return '<div class="notice"><h3>'. $title .'</h3><p>' . do_shortcode($content) . '</p></div>';
}
add_shortcode('notice', 'notice_shortcode');

/* Code Example
[notice title="Important Note"]Make-up exams or Individual placement exams are not offered, please plan accordingly.[/notice]
*/

// Hide email from Spam Bots using shortcode
function hide_email_shortcode( $atts , $content = null ) {
	if ( ! is_email( $content ) ) {
		return;
	}
	return '<a href="mailto:' . antispambot( $content ) . '">' . antispambot( $content ) . '</a>';
}
add_shortcode( 'email', 'hide_email_shortcode' );

/* Code Example
[email]bruin@humnet.ucla.edu[/email]
*/

/*
// Language Shortcode
// Use the below code to let screen readers know when content is in a different language
// replace "es" with proper language code (http://www.w3schools.com/tags/ref_language_codes.asp)

// Spanish
function spanish_shortcode( $atts, $content = null ) {
	return '<span lang="es">' . $content . '</span>';
}
add_shortcode( 'spanish', 'spanish_shortcode' );
*/

/* Code Example
[spanish]Deme un momento por favor.[/spanish]
*/

/************* HIDING SPECIFIC PAGES FROM INTERNAL SEARCH *********************/

/*
function search_filter( $query ) {
	if ( $query->is_search && $query->is_main_query() ) {
		$query->set( 'post__not_in', array( 626,617 ) ); 
	}
}
add_filter( 'pre_get_posts', 'search_filter' );
*/

/************* MAKING IMAGE CAPTIONS IN HTML5 *********************/

add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );

function cleaner_caption( $output, $attr, $content ) {
	if ( is_feed() )
		return $output;
	/* Set up the default arguments. */
	$defaults = array(
		'id' => '',
		'align' => 'alignnone',
		'width' => '',
		'caption' => ''
	);

	/* Merge the defaults with user input. */
	$attr = shortcode_atts( $defaults, $attr );

	/* If the width is less than 1 or there is no caption, return the content wrapped between the [caption]< tags. */
	if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
		return $content;

	/* Set up the attributes for the caption <div>. */
	$attributes = ( !empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
	$attributes .= ' class="wp-caption ' . esc_attr( $attr['align'] ) . '"';
	$attributes .= ' style="width: ' . esc_attr( $attr['width'] ) . 'px"';

	/* Open the caption. */
	$output = '<figure' . $attributes .'>';

	/* Allow shortcodes for the content the caption was created for. */
	$output .= do_shortcode( $content );

	/* Append the caption text. */
	$output .= '<figcaption class="wp-caption-text">' . $attr['caption'] . '</figcaption>';

	/* Close the caption. */
	$output .= '</figure>';

	/* Return the formatted, clean caption. */
	return $output;
}

/************* SETTING EXCERPT STRING *********************/

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

/************* GOOGLE FONTS *********************/

/*
If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/
function bones_fonts() {
	wp_enqueue_style('googleFonts', '//fonts.googleapis.com/css?family=Raleway:400,500,600,700');
}
add_action('wp_enqueue_scripts', 'bones_fonts');

/************* GOOGLE MAP API KEY *********************/

/* You shold replace the below with a Google API Key you can get here: 
https://developers.google.com/maps/documentation/javascript/get-api-key
*/

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyCEMkObqkoiTFlrQaL2UatDddDEZ1UrpmA');
}
add_action('acf/init', 'my_acf_init');

/************* FANCYBOX *********************/

// Gallery images
function ccd_fancybox_gallery_attribute( $content, $id ) {
	// Restore title attribute
	$title = get_the_title( $id );
	return str_replace('<a', '<a data-type="image" data-fancybox="gallery" title="' . esc_attr( $title ) . '" ', $content);
}
add_filter( 'wp_get_attachment_link', 'ccd_fancybox_gallery_attribute', 10, 4 );

// Single images
function ccd_fancybox_image_attribute( $content ) {
	global $post;
	$pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
	$replace = '<a$1href=$2$3.$4$5 data-type="image" data-fancybox="image">';
	$content = preg_replace( $pattern, $replace, $content );
	return $content;
}
add_filter( 'the_content', 'ccd_fancybox_image_attribute' );

/************* CUSTOM LOGO *********************/

function customlogo_setup() {
    add_theme_support('custom-logo', array(
		'height'      => 73,
		'width'       => 581,
		'flex-height' => true,
		'flex-width'  => true,
	));
}
add_action('after_setup_theme', 'customlogo_setup');
add_filter('get_custom_logo','change_logo_class');

function change_logo_class($html) {
	$html = str_replace('class="custom-logo"', '', $html);
	$html = str_replace('class="custom-logo-link"', '', $html);
	return $html;
}

/************* EVENTS *********************/
/* This snippet will show the bookings private notes to the Bookings Summary page at Events > Bookings
After adding in your functions.php, go to Events > Bookings > Recent Bookings or by clicking an specific. Click on the Gear icon and drag&drop field "Private Notes"
*/
function my_em_bookings_table_cols_template_attendee2($template, $EM_Bookings_Table){
	$template['private_notes'] = 'Private Notes';
	return $template;
}
add_action('em_bookings_table_cols_template', 'my_em_bookings_table_cols_template_attendee2',10,2);

function my_em_booking_notes_template($val, $col, $EM_Booking, $EM_Bookings_Table, $csv){
	
	if( $col == 'private_notes' ){
		$temp = $EM_Booking->get_notes();
		$val .= '<ul>';
		foreach ($temp as $key => $value):
			$author = get_the_author_meta( 'display_name', $temp[$key]['author'] );
			$val .= '<li><ul>';
			$val .= '<li>'.'Author: '.$author.'</li>';
			$val .= '<li>'.'Notes: '.$temp[$key]['note'].'</li>';
			$val .= '</ul></li>';
		endforeach;
		$val .= '</ul>';
	}
	
	return $val;
}
add_filter('em_bookings_table_rows_col','my_em_booking_notes_template', 10, 5);

// This snippet will add support to post revisions at event CPT

function em_mod_support($supports){
	$supports = array('custom-fields','title','editor','excerpt','comments','thumbnail','author','revisions');
	return $supports;
}
add_filter('em_cp_event_supports','em_mod_support',1,1);

/************* GRAVITY FORMS *********************/
/**
 * Gravity Wiz // Gravity Forms // Send Manual Notifications
 *
 * Provides a custom notification event that allows you to create notifications that can be sent
 * manually (via Gravity Forms "Resend Notifications" feature).
 *
 * @version   1.1
 * @author    David Smith <david@gravitywiz.com>
 * @license   GPL-2.0+
 * @link      http://gravitywiz.com/send-manual-notifications-with-gravity-forms/
 */
class GW_Manual_Notifications {

    private static $instance = null;

    public static function get_instance() {
        if( null == self::$instance )
            self::$instance = new self;
        return self::$instance;
    }

    private function __construct() {

	    add_filter( 'gform_notification_events', array( $this, 'add_manual_notification_event' ) );

	    add_filter( 'gform_before_resend_notifications', function( $form ) {
			add_filter( 'gform_notification', array( $this, 'evaluate_notification_conditional_logic' ), 10, 3 );
		    return $form;
	    } );

    }

	public function add_manual_notification_event( $events ) {
		$events['manual'] = __( 'Send Manually' );
		return $events;
	}

	public function evaluate_notification_conditional_logic( $notification, $form, $entry ) {

		// if it fails conditional logic, suppress it
		if( $notification['event'] == 'manual' && ! GFCommon::evaluate_conditional_logic( rgar( $notification, 'conditionalLogic' ), $form, $entry ) ) {
			add_filter( 'gform_pre_send_email', array( $this, 'abort_next_notification' ) );
		}

		return $notification;
	}

	public function abort_next_notification( $args ) {
		remove_filter( 'gform_pre_send_email', array( $this, 'abort_next_notification' ) );
		$args['abort_email'] = true;
		return $args;
	}

}

function gw_manual_notifications() {
    return GW_Manual_Notifications::get_instance();
}

gw_manual_notifications();

// Unique Form ID

// update the 93 here to your form ID
add_action('gform_after_submission_3', 'add_submission_id', 10, 2);
function add_submission_id($entry, $form) {
    global $wpdb;
    // update this to the number of the hidden field in your form
    $field_number = 35;
    // your submission ID format to be inserted into the hidden field
    $SubmissionID = '1718' . $entry['id'];
    // update the entry
    $wpdb->insert("{$wpdb->prefix}rg_lead_detail", array(
        'value'         => $SubmissionID,
        'field_number'  => $field_number,
        'lead_id'       => $entry['id'],
        'form_id'       => $entry['form_id']
    ));
}

/************* EMAIL SETTINGS *********************/

add_filter( 'wp_mail_from', 'your_email' );
function your_email( $original_email_address ) {
	return 'c1718cs@humnet.ucla.edu';
}
add_filter( 'wp_mail_from_name', 'custom_wp_mail_from_name' );
function custom_wp_mail_from_name( $original_email_from ) {
	return 'UCLA Center for 17th- & 18th-Century Studies';
}

/************* RSS FEED EDITS *********************/

// Remove images
function remove_images( $the_excerpt_rss ) {
$postOutput = preg_replace('/<img[^>]+./','', $the_excerpt_rss);
return $postOutput;}
add_filter( 'the_excerpt_rss', 'remove_images', 100 );

/************* 404 REDIRECT *********************/

function remove_redirect_guess_404_permalink( $redirect_url ) {
    if ( is_404() )
        return false;
    return $redirect_url;
}

add_filter( 'redirect_canonical', 'remove_redirect_guess_404_permalink' );

/* DON'T DELETE THIS CLOSING TAG */ ?>