<?php get_header(); ?>

			<?php // include("slider.php"); ?>
			<?php
				$image = get_field('main_image');
				if( !empty($image) ): 
					// vars
					$url = $image['url'];
					$title = $image['title'];
					// thumbnail
					$size = 'hero';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ];
				endif; ?>
			<div id="hero" style="background-image:url('<?php echo $thumb; ?>');">
				<div class="content">
				<?php if(get_field('main_text')) { ?>
					<blockquote>
						<?php the_field('main_text'); ?>
						<?php if(get_field('main_text_source')) { ?>
						<cite>&mdash; <?php the_field('main_text_source'); ?></cite>
						<?php } ?>
					
					</blockquote>
				<?php } ?>
				</div>
			</div>
			<div class="content">
				<div class="col">
					<?php $about_query = new WP_Query( 'pagename=about' );
					while ( $about_query->have_posts() ) : $about_query->the_post();
					$do_not_duplicate = $post->ID; ?>
					<h2><?php the_title(); ?></h2>
					<p><?php
					$content = get_the_content();
					$trimmed_content = wp_trim_words( $content, 100, '...' );
					echo $trimmed_content;
					?></p>
					<a class="btn" href="<?php the_permalink() ?>">Learn More</a>
					<?php endwhile; ?>
				</div>
				<div class="col">
					<h3>Quick Links</h3>
					<?php wp_nav_menu(array(
						'container' => '',
						'menu' => __( 'Quick Links', 'bonestheme' ),
						'menu_class' => 'footer-nav',
						'theme_location' => 'quick-links',
						'before' => '',
						'after' => '',
						'link_before' => '<h4>',
						'link_after' => '</h4>',
						'depth' => 0,
						'walker' => new Description_Walker
					)); ?>
				</div>
				<div class="col">
					<h3>News & Events</h3>
					<ul>
						<?php $news_query = new WP_Query( 
						array(
							'post_type'=> array('post', 'tribe_events'),
							'posts_per_page' => 5,
							'order' => asc
						));
						while ( $news_query->have_posts() ) : $news_query->the_post();
						$do_not_duplicate = $post->ID; ?>
						<li>
							<?php if ( 'tribe_events' == get_post_type() ) : ?>
							<span class="event-category">Event</span>
							<?php else : ?>
							<span class="news-category">News</span>
							<?php endif; ?>
							<a href="<?php the_permalink() ?>"><h4><?php the_title(); ?></h4>
							<span><?php
							$content = get_the_content();
							$trimmed_content = wp_trim_words( $content, 20, '...' );
							echo $trimmed_content;
							?></span></a>
						</li>								
						<?php endwhile; ?>
					</ul>
					<a class="btn" href="/news/">View All</a>
				</div>
			</div>
<?php get_footer(); ?>