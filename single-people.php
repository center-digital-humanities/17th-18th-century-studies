<?php
/*
 * PEOPLE POST TYPE TEMPLATE
*/
?>

<?php get_header(); ?> 
			
				<div class="content main">
					<div class="col" id="main-content" role="main">
						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">
							<h1 id="bio"><?php the_title(); ?></h1>
							<div class="details">
								<?php if(get_field('email_address')) { ?>
									<span><strong>E-mail: </strong><a href="mailto:<?php the_field('email_address'); ?>"><?php the_field('email_address'); ?></a></span>
								<?php } ?>
								<?php if(get_field('phone_number')) { ?>
									<span><strong>Phone: </strong><?php the_field('phone_number'); ?></span>
								<?php } ?>
								<?php if(get_field('office')) { ?>
									<span><strong>Office: </strong><?php the_field('office'); ?></span>
								<?php } ?>
								<?php if(get_field('office_hours')) { ?>
									<p><strong>Office Hours:</strong> <?php the_field('office_hours'); ?></p>
								<?php } ?>
							</div>
							<section class="bio">
								<?php the_content(); ?>
							</section>
							<?php if(get_field('education')) { ?>
							<section id="education">
								<h2>Education</h2>
								<?php the_field('education'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('research')) { ?>
							<section id="research">
								<h2>Research</h2>
								<?php the_field('research'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('publications')) { ?>
							<section id="publications">
								<h2>Publications</h2>
								<?php the_field('publications'); ?>
							</section>
							<?php } ?>
							<?php if(get_field('articles')) { ?>
							<section id="articles">
								<h2>Articles</h2>
								<?php the_field('articles'); ?>
							</section>
							<?php } ?>
						</article>
	
						<?php endwhile; ?>

						<?php else : ?>

						<article id="post-not-found" class="hentry cf">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
							</header>
							<section class="entry-content">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
							</section>
							<footer class="article-footer">
								<p><?php _e( 'This is the error message in the single-custom_type.php template.', 'bonestheme' ); ?></p>
							</footer>
						</article>

						<?php endif; ?>
	
					</div>
					<div class="col">
						<?php if(get_field('photo')) {
							$image = get_field('photo');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'people-large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="photo
							<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } else { ?>
							<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="Silhouette" width="300px" height="300px" class="photo
							<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
							<?php } ?>						
						<div class="content col-nav">
							<nav role="navigation" aria-labelledby="person navigation">
								<?php if(get_field('education') || get_field('research') || get_field('courses') || get_field('books') || get_field('articles')) { ?>
									<h3>Table of Contents</h3>
									<ul>
										<?php if( empty( $post->post_content) ) {
											// Do nothing
										} else { ?>
										<li><a href="#bio">Bio</a></li>
										<?php } ?>
										<?php if(get_field('education')) { ?>
										<li><a href="#education">Education</a></li>
										<?php } ?>
										<?php if(get_field('research')) { ?>
										<li><a href="#research">Research</a></li>
										<?php } ?>
										<?php if(get_field('courses')) { ?>
										<li><a href="#courses">Current Courses</a></li>
										<?php } ?>
										<?php if(get_field('publications')) { ?>
										<li><a href="#books">Publications</a></li>
										<?php } ?>
										<?php if(get_field('articles')) { ?>
										<li><a href="#articles">Articles</a></li>
										<?php } ?>
									</ul>
								<?php }
								if(get_field('cv') || get_field('personal_website') || get_field('academia_profile') || get_field('additional_link')) { ?>
									<h3>Additional Links</h3>
									<ul>
										<?php if(get_field('cv')) { ?>
										<li><a href="<?php the_field('cv'); ?>" class="download">Download CV</a></li>
										<?php } ?>
										<?php if(get_field('personal_website')) { ?>
										<li><a href="<?php the_field('personal_website'); ?>" class="link">Personal Website</a></li>
										<?php } ?>
										<?php if(get_field('academia_profile')) { ?>
										<li><a href="<?php the_field('academia_profile'); ?>" class="link">Academia Profile</a></li>
										<?php } ?>
										<?php if(get_field('additional_link')) { ?>
										<li><a href="<?php the_field('additional_link'); ?>" class="link"><?php the_field('additional_link_title'); ?></a></li>
										<?php } ?>
									</ul>
								<?php } ?>
							</nav>
						</div>
					</div>	
				</div>

<?php get_footer(); ?>
