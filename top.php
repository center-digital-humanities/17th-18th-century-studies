<?php
/*
	Basic Navigation and Hero 
*/
?>

<a href="#main-content" class="hidden skip">Skip to main content</a>
<div id="container">
	<header role="banner" class="top">
		<div class="content">
			<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.svg" alt="UCLA" class="university-logo" /></a>
			<div class="dept-logo">             
			    <?php // Custom Logo code start
				if (has_custom_logo()) {  // Display the Custom Logo
					the_custom_logo();
				} else {  // No Custom Logo, just display the site's name ?>
				<a href="<?php echo home_url(); ?>"><h1 class="logo-text" alt="<?php the_field('department_name', 'option'); ?>"><?php bloginfo('name'); ?></h1></a>
				<?php } // Custom Logo code ends ?>
			</div>
			<nav role="navigation" aria-label="Main Navigation" class="desktop">
				<?php wp_nav_menu(array(
					'container' => false,
					'menu' => __( 'Main Menu', 'bonestheme' ),
					'menu_class' => 'main-nav',
					'theme_location' => 'main-nav',
					'before' => '',
					'after' => '',
					'depth' => 2,
				)); ?>
			</nav>
		</div>
	</header>
	
<?php // For event pages
if (is_tree(4) || is_singular ( 'event') || is_page(6)) { ?>
	<div class="nav-container">
		<div class="content">
			<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
				<?php 
					wp_nav_menu(array(
						'container' => false,
						'menu' => __( 'Events', 'bonestheme' ),
						'menu_class' => 'events-nav',
						'theme_location' => 'events-nav',
						'before' => '',
						'after' => '',
						'depth' => 1,
						'items_wrap' => '<h3>Events</h3><ul>%3$s</ul>'
					));
				?>
			</nav>
		</div>
	</div>
	<?php 
	if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
	}
} else { 
	if ( is_front_page() ) { } else { 
	// For everything else
	?>
		<div class="nav-container">
			<div class="content">
				<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
					<?php 
						// If a Research subpage
						if (is_tree(939) || get_field('menu_select') == "research") {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Research', 'bonestheme' ),
								'menu_class' => 'research-nav',
								'theme_location' => 'research-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>Research</h3><ul>%3$s</ul>'
							));
						}
						
						// If a People subpage
						if (is_tree(1096) || get_field('menu_select') == "people" ) {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'People', 'bonestheme' ),
								'menu_class' => 'people-nav',
								'theme_location' => 'people-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>People</h3><ul>%3$s</ul>'
							));
						}
						
						// If a Newsletter subpage
						if (get_field('menu_select') == "newsletter" ) {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Newsletter', 'bonestheme' ),
								'menu_class' => 'newsletter-nav',
								'theme_location' => 'newsletter-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>Newsletter</h3><ul>%3$s</ul>'
							));
						}
						
						// If a Giving subpage
						if ( get_field('menu_select') == "giving" ) {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Giving', 'bonestheme' ),
								'menu_class' => 'giving-nav',
								'theme_location' => 'giving-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>Giving</h3><ul>%3$s</ul>'
							));
						}
						
						// If a About subpage
						if ( get_field('menu_select') == "about" ) {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'About', 'bonestheme' ),
								'menu_class' => 'about-nav',
								'theme_location' => 'about-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>About</h3><ul>%3$s</ul>'
							));
						}
						
						// If a Contact subpage
						if ( get_field('menu_select') == "contact" ) {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Contact', 'bonestheme' ),
								'menu_class' => 'contact-nav',
								'theme_location' => 'contact-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>Contact</h3><ul>%3$s</ul>'
							));
						}
						
						// If a blog post or category
						if ( is_single() || is_category() ) {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'News', 'bonestheme' ),
								'menu_class' => 'news-nav',
								'theme_location' => 'news-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>Categories</h3><ul>%3$s</ul>'
							));
						}
						
						// For Search, 404's, or other pages you want to use it on
						// Replace 9999 with id of parent page
						if ( is_tree(9999) || is_search() || is_404() || get_field('menu_select') == "general") {
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'About', 'bonestheme' ),
								'menu_class' => 'about-nav',
								'theme_location' => 'about-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>About</h3><ul>%3$s</ul>'
							));
						}
						
						// If user doesn't want a sub menu
						if ( get_field('menu_select') == "custom" ) { ?>
							<h3><?php the_field('menu_title'); ?></h3><!--<ul>&nbsp;</ul>-->
						<?php } 
					?>
				</nav>
			</div>
		</div>
		<?php 
		if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
		}
	}
} ?>