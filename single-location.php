<?php get_header(); ?>
			<div class="nav-container">
				<div class="content">
					<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
						<?php 
							wp_nav_menu(array(
								'container' => false,
								'menu' => __( 'Events', 'bonestheme' ),
								'menu_class' => 'events-nav',
								'theme_location' => 'events-nav',
								'before' => '',
								'after' => '',
								'depth' => 1,
								'items_wrap' => '<h3>Events</h3><ul>%3$s</ul>'
							));
						?>
					</nav>
				</div>
			</div>
			<?php 
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
				} 
			?>
			<div class="content">
				<div class="col" id="main-content" role="main">

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_content(); ?>
						</section>
					</article>

				<?php endwhile; ?>

				<?php else : ?>

					<article id="post-not-found" class="hentry cf">
						<header class="article-header">
							<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
						<section class="entry-content">
							<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
							<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

				<?php endif; ?>
				
				</div>
			</div>

<?php get_footer(); ?>
