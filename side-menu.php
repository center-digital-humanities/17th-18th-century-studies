<?php 
	// If a Graduate subpage or Group page
	if (is_tree(863) || get_field('menu_select') == "graduate" || get_post_type() == "groups_type") {
		wp_nav_menu(array(
			'container' => false,
			'menu' => __( 'Graduate', 'bonestheme' ),
			'menu_class' => 'graduate-nav',
			'theme_location' => 'graduate-nav',
			'before' => '',
			'after' => '',
			'depth' => 2,
			'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
		));
	}
	
	// If an Undergraduate subpage
	if (is_tree(860) || get_field('menu_select') == "undergraduate") {
		wp_nav_menu(array(
		   	'container' => false,
		   	'menu' => __( 'Undergraduate', 'bonestheme' ),
		   	'menu_class' => 'undergrad-nav',
		   	'theme_location' => 'undergrad-nav',
		   	'before' => '',
		   	'after' => '',
		   	'depth' => 2,
		   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
		));
	}
	
	// For Search, 404's, or other pages you want to use it on
	// Replace 9999 with id of parent page
	if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
		wp_nav_menu(array(
			'container' => false,
			'menu' => __( 'Main Menu', 'bonestheme' ),
			'menu_class' => 'side-nav',
			'theme_location' => 'main-nav',
			'before' => '',
			'after' => '',
			'depth' => 1,
			'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
		));
	}
?>