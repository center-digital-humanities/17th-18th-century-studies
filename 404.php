<?php get_header(); ?>

			<div class="content">
				<div class="col" id="main-content" role="main">
					<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1>Page Not Found</h1>
						<section>
							<p>Sorry but the page you are looking for is not here. Try starting from the <a href="<?php echo home_url(); ?>">homepage</a>.</p>
						</section>
					</article>
				</div>
				<?php get_sidebar(); ?>
			</div>

<?php get_footer(); ?>
